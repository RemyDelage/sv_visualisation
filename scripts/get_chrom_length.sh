#!/bin/bash

# Variables
directory=$(pwd)
tsv_files=($directory/*.tsv)
vcf_file=$(find . -name "*.vcf")

# Extraction des informations du VCF dans un fichier temporaire
grep '##contig' $vcf_file | awk -F 'ID=|,|length=' '{print $2 "\t" $3}' > contig_info.txt

# Création d'un fichier de correspondance CHROM - LENGTH
awk 'NR>1 {print $1}' $contig_info.txt > contig_chroms.txt
awk 'NR>1 {print $2}' $contig_info.txt > contig_lengths.txt

# Tri des fichiers de correspondance
sort -k1,1 contig_chroms.txt -o contig_chroms.txt
sort -k1,1 contig_lengths.txt -o contig_lengths.txt

# Parcourir les fichiers TSV
for tsv_file in ${tsv_files[@]}; do
    tsv_file_name=$(basename $tsv_file .tsv)
    new_tsv_file="$tsv_file_name"_new.tsv

    # Création de l'en-tête pour le nouveau fichier TSV
    echo -e "CHROM\tLENGTH" > $new_tsv_file

    # Utilisation de paste pour fusionner les fichiers de correspondance CHROM et LENGTH
    paste -d'\t' contig_chroms.txt contig_lengths.txt > contig_map.txt

    # Tri du fichier TSV par la colonne CHROM avant la jointure
    sort -k1,1 $tsv_file -o $tsv_file

    # Recherche de la correspondance entre CHROM dans le fichier TSV trié et le fichier de correspondance
    join -1 1 -2 1 $tsv_file contig_map.txt | cut -f 2,3 >> $new_tsv_file
done

# Nettoyer les fichiers temporaires
rm -f contig_info.txt contig_chroms.txt contig_lengths.txt contig_map.txt

