#!/bin/bash

############################################################
# This script converts .vcf files into .tsv files with and without BND entries.
#
# TOOLS:
#   bcftools (version 1.3)
#
# OPTIONS:
#   bcftools view : subset, filter, and convert VCF and BCF files
#       -e : exclude sites for which EXPRESSION is true.
#
#   bcftools query : transform VCF/BCF into user-defined formats (here: tsv)
#       -f : Skip sites where FILTER column does not contain any of the strings listed in the list.
############################################################

## Variables
directory=$(pwd)
files_names=($directory/*.vcf)

## Code
# for loop for running this code directly for all the files
for file in ${files_names[@]} ; do
    	# Extract the name of the file
      	file_tsv=$(basename $file .vcf)

	# Remove the "Breakend" SVs type
        bcftools view -e 'INFO/SVTYPE="BND"' $file >> ${file_tsv}_no_bnd.vcf

	if [[ $file == *cutesv* ]]; then

    		# Create the header of the tabulated file
    		echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tGENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANTS_READS\tREADS_SUPPORT\tALLELE_FREQUENCY" > ${file_tsv}_no_bnd.tsv
    		echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tGENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANTS_READS\tREADS_SUPPORT\tALLELE_FREQUENCY" > ${file_tsv}.tsv

    		# Extract the informations from the VCF file and create the tabulated file
    		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t[%GQ]\t[%DR]\t[%DV]\t%INFO/RE\t%INFO/AF\n' ${file_tsv}_no_bnd.vcf >> ${file_tsv}_no_bnd.tsv
    		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t[%GQ]\t[%DR]\t[%DV]\t%INFO/RE\t%INFO/AF\n' ${file_tsv}.vcf >> ${file_tsv}.tsv

	fi

	if [[ $file == *sniffles* ]]; then

		# Create the header of the tabulated file
                echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tGENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANTS_READS\tREADS_SUPPORT\tALLELE_FREQUENCY" > ${file_tsv}_no_bnd.tsv
                echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tGENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANTS_READS\tREADS_SUPPORT\tALLELE_FREQUENCY" > ${file_tsv}.tsv

		 # Extract the informations from the VCF file and create the tabulated file
                bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t[%GQ]\t[%DR]\t[%DV]\t%INFO/SUPPORT\t%INFO/AF\n' ${file_tsv}_no_bnd.vcf >> ${file_tsv}_no_bnd.tsv
		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t[%GQ]\t[%DR]\t[%DV]\t%INFO/SUPPORT\t%INFO/AF\n' ${file_tsv}.vcf >> ${file_tsv}.tsv
	fi
	if [[ $file == *svim* ]]; then

		# Create the header of the tabulated file
        	echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tREADS_SUPPORT\tREADS_DEPTH\tRD_FOR_EACH_ALLELE" > ${file_tsv}_no_bnd.tsv
        	echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tREADS_SUPPORT\tREADS_DEPTH\tRD_FOR_EACH_ALLELE" > ${file_tsv}.tsv

		# Extract the informations from the VCF file and create the tabulated file
		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t%INFO/SUPPORT\t[%DP]\t[%AD]\n' ${file_tsv}_no_bnd.vcf >> ${file_tsv}_no_bnd.tsv
		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[%GT]\t%INFO/SUPPORT\t[%DP]\t[%AD]\n' ${file_tsv}.vcf >> ${file_tsv}.tsv

	fi
	if [[ $file == *merged* ]]; then
    		# Create the header of the tabulated file
    		echo -e "CHROM\tPOS\tEND\tTYPE\tLENGTH\tGENOTYPE\tHQ_REFERENCE_READS\tHQ_VARIANTS_READS\tREADS_SUPPORT\tTOOLS" > ${file_tsv}.tsv

    		# Extract the information from the VCF file and create the tabulated file
    		bcftools query -f '%CHROM\t%POS\t%INFO/END\t%INFO/SVTYPE\t%INFO/SVLEN\t[ %GT]\t[ %DR]\t[ %DV]\t%INFO/SUPPORT\t%INFO/IDLIST\n' ${file_tsv}.vcf >> ${file_tsv}.tsv
	fi

	# Remove the files which we don't need :
	rm -f *_no_bnd_no_bnd*
	rm -f *merged_noBND_no_bnd*
done
date
