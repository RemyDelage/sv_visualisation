import os
import glob

# Variables
directory = os.getcwd() # The files has to be in the same directory than the script
file = "ERR6608653_cutesv.vcf" 
# We can choose only one VCF file because all the VCF files contains the same 
# information about the chromosomes lengths.

# Code
print("Work in progress...")

with open(file, "r") as vcf:
    # Create empty list for store the chomosome and this length
    chrom_length_data=[]
        # Extract the information from VCF file
    for line in vcf:
        # Checking the lines of the header that contains contig informations
        if line.startswith("##contig="):
            # Split the header line in two parts with "ID=" and "," 
            # as delimiter
            info = line.split("ID=")
            if len(info) == 2:
                chrom, length = info[1].split(",", 1)
                # Drop the str "length=" and ">" of the line
                length = length.replace("length=", "").replace(">", "")
                # Store the informations of chrom and length into a list 
                # that will be used for writing new TSV file 
                chrom_length_data.append((chrom, length))
        
    # Create the new TSV file name
    new_tsv_file = "chromosomes_length.tsv"
        
        # Writing the extracted informations into TSV file
    with open(new_tsv_file, "w") as tsv:
        # Writing the header
        tsv.write("CHROM\tLENGTH\n")
        # Writing the extracted informations from VCF to TSV
        for chrom, length in chrom_length_data:
            tsv.write(f"{chrom}\t{length}\n")

print("Complete")

            