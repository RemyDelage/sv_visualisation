#!/bin/bash

# Variables
directory=$(pwd)
tsv_files=($directory/*.tsv)

## Code
for tsv_file in "${tsv_files[@]}"; do
        file=$(basename "$tsv_file" .tsv)
        vcf_file="${file}.vcf"

        if [[ $file == *cutesv* || $file == *sniffles*  ]]; then
                echo -e "GENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANT_READS" > "${file}_2.tsv"
                echo -e "GENOTYPE_QUALITY\tHQ_REFERENCE_READS\tHQ_VARIANT_READS" > "${file}_2_no_bnd.tsv"

                bcftools query -f '[%GT]\t[%DR]\t[%DV]\n' "$vcf_file" > "${file}_2.tsv"
                bcftools query -f '[%GT]\t[%DR]\t[%DV]\n' "${file}_no_bnd.vcf" > "${file}_2_no_bnd.tsv"

                paste -d '\t' "$tsv_file" "${file}_2.tsv" > "${file}_updated.tsv"
                paste -d '\t' "${file}_no_bnd.tsv" "${file}_2_no_bnd.tsv" > "${file}_updated_no_bnd.tsv"

                mv "${file}_updated.tsv" "$tsv_file"
                mv "${file}_updated_no_bnd.tsv" "${file}_no_bnd.tsv"
        fi
done
date




#for tsv_file in "${tsv_files[@]}"; do
#	# Create new colum GENOTYPE_QUALITY into tsv files that contains NA
#	sed -i '1s/\tGENOTYPE$/\tGENOTYPE\tGENOTYPE_QUALITY/' "$tsv_file"
#    	awk 'BEGIN { OFS="\t" } { $0 = $0 "\tNA" } 1' "$tsv_file" > temp.tsv
#    	mv temp.tsv "$tsv_file"
#
#	if [[ "$tsv_file" != *svim* ]]; then
#      	new=$(bcftools query -f '[%GQ]\n' "${tsv_file%.tsv}.vcf")
#	i=1
#	while read -r new_value; do
#		awk - v new_value=$new_value 'BEGIN { OFS="\t" } { if ($NF == "NA") $NF = new_value } 1' $tsv_file > "temp2.tsv"
#       		mv "temp2.tsv" $tsv_file
#		i=$((i+1))
#	done <<< $new_values
#	mv "temp2.tsv" $tsv_file
#	fi
#done
